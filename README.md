# Ubuntu initialization script
This is the script to install & config a Ubuntu working machine. #DRY.
*Note* 
This only works on a freshly installed system. 

## How to use
```
cd ~
git clone https://RobertTheNerd@bitbucket.org/RobertTheNerd/ubuntu-dev-post-install.git 
sh ubuntu-dev-post-install/bin/post-install.sh
rm -rf ubuntu-dev-post-install
```
